(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var Session = require('./lib/session')
  , JID = require('./lib/jid')
  , sasl = require('./lib/sasl')
  , Anonymous = require('./lib/authentication/anonymous')
  , Plain = require('./lib/authentication/plain')
  , XOAuth2 = require('./lib/authentication/xoauth2')
  , External = require('./lib/authentication/external')
  , util = require('./lib/util')
  , Base64 = require('./lib/base64').Base64
  , element  = require('./lib/element')

var Group = element.Group
  , Stanza =  element.Stanza
  , Iq =  element.Iq
  , Message = element.Message
  , Element = element.Element
  , Presence = element.Presence

var logger =  util.logger;

var STATE_PREAUTH = 0
  , STATE_AUTH = 1
  , STATE_AUTHED = 2
  , STATE_BIND = 3
  , STATE_SESSION = 4
  , STATE_ONLINE = 5

var IQID_SESSION = 'sess'
  , IQID_BIND = 'bind'

function Client(options) {
    this.options = {}
    if (options) this.options = options
    this.availableSaslMechanisms = [
        XOAuth2, External, Plain, Anonymous
    ]

    if (this.options.autostart !== false)
        this.connect()
}

util.inherits(Client, Session)

Client.NS_CLIENT = Stanza.NAMESPACE.NS_CLIENT


/* @TODO:
    We should prefer native atob and btoa over  Base64.decode and Base64.encode
	search if latest GC engine supports these functions
*/

/* jshint latedef: false */
/* jshint -W079 */
/* jshint -W020 */

var decode64, encode64;

decode64 = Base64.decode;
encode64 = Base64.encode;


/*if (typeof btoa === 'undefined') {
    var btoa = null
    var atob = null
}

if (typeof btoa === 'function') {
    decode64 = function(encoded) {
        return atob(encoded)
    }
} else {
    logger.error('No atob :',atob);
}
if (typeof atob === 'function') {
    encode64 = function(decoded) {
        return btoa(decoded)
    }
} else {
    logger.warn('No btoa :',btoa);
}*/

Client.prototype.connect = function() {

        this.options.xmlns = Stanza.NAMESPACE.NS_CLIENT
        /* jshint camelcase: false */
        delete this.did_bind
        delete this.did_session

        this.state = STATE_PREAUTH
        this.on('end', function() {
            this.state = STATE_PREAUTH
            delete this.did_bind
            delete this.did_session
        })

        this.on('error', function(err) {
            logger.error("Error occured while Connecting JMPP Client",err);
        });

        Session.call(this, this.options)
        this.options.jid = this.jid

        this.connection.on('disconnect', function(error) {
            this.state = STATE_PREAUTH
            if (!this.connection.reconnect) {
                if (error) this.emit('error', error)
                this.emit('offline')
            }
            delete this.did_bind
            delete this.did_session
        }.bind(this))

        // If server and client have multiple possible auth mechanisms
        // we try to select the preferred one
        if (this.options.preferred) {
            this.preferredSaslMechanism = this.options.preferred
        } else {
            this.preferredSaslMechanism = 'External'
        }

        var mechs = sasl.detectMechanisms(this.options, this.availableSaslMechanisms)
        this.availableSaslMechanisms = mechs
}

Client.prototype.onStanza = function(stanza) {
    var stanzaId = stanza.attrs.id;
    var to = stanza.attrs.to || '';

    if ((this.state !== STATE_ONLINE) && stanza.is('features')) {
        this.streamFeatures = stanza
        this.useFeatures()
    } else if (this.state === STATE_PREAUTH) {
        this.emit('stanza:preauth', stanza)
    } else if (this.state === STATE_AUTH) {
        this._handleAuthState(stanza)
    } else if ((this.state === STATE_BIND) && stanza.is('iq') && (stanzaId === IQID_BIND)) {
        this._handleBindState(stanza)
    } else if ((this.state === STATE_SESSION) && (true === stanza.is('iq')) &&
        (stanzaId === IQID_SESSION)) {
        this._handleSessionState(stanza)
    } else if (stanza.name === 'stream:error') {
        if (!this.reconnect){
            this.emit('error', stanza)
        }
    } else if (this.state === STATE_ONLINE) {
        var type = stanza.attrs.type;;

        if(!stanza.is( Stanza.TYPE.GROUP)){
            this.emit('stanza', stanza);
        }
        /*
            Reply acknowldgment for Message & Group stanza receipnt
        */
        var replyStanzaId;

        if(stanza.is(Stanza.TYPE.MESSAGE)){

            if(!type){
                type = Message.TYPE.MESSAGE;
            }

            var ackMsg;
            if(type == Message.TYPE.MESSAGE) {
                replyStanzaId = Message.prototype.generateID(Message.TYPE.USER_RECEIVE_ACK);
                var fromBareAddress = stanza.attrs.from && new JID(stanza.attrs.from).bare().toString();
                ackMsg = Message.create({
                    to: fromBareAddress,
                    type: Message.TYPE.USER_RECEIVE_ACK,
                    created: Date.now(),
                    ackid: stanzaId,
                    id: replyStanzaId
                });
                this.send(ackMsg);
            }
        } else if(stanza.is( Stanza.TYPE.GROUP) && type == Group.TYPE.STANZA_GROUP){
            var groupStanzas = stanza.getChildElements();
            var totalStanza = groupStanzas.length;

            if(totalStanza > 0){
                var childStanza;
                var groupStanzaReply = Group.create({
                    type: Group.TYPE.STANZA_GROUP_ACK,
                    created: Date.now(),
                    ackid: stanzaId,
                    id: Message.prototype.generateID(Group.TYPE.STANZA_GROUP_ACK)
                });

                for(var i = 0; i < totalStanza; i++){
                    if(groupStanzas[i]){
                         try{
                            childStanza = Stanza.prototype.parse(groupStanzas[i])
                         }catch(e){
                            logger.error("Group stanza parsing error",e);
                         };

                         this.emit('stanza',childStanza);

                         var childType = childStanza.attrs.type || Message.TYPE.MESSAGE;
                         if(childStanza.is(Stanza.TYPE.MESSAGE) && childType == Message.TYPE.MESSAGE){
                            var fromBareAddress = childStanza.attrs.from && new JID(childStanza.attrs.from).bare().toString();
                            groupStanzaReply.c(Stanza.TYPE.MESSAGE,{
                                to: fromBareAddress,
                                type: Message.TYPE.USER_RECEIVE_ACK,
                                created: Date.now(),
                                ackid: childStanza.attrs.id,
                                id:  Message.prototype.generateID(Message.TYPE.USER_RECEIVE_ACK)
                            }).up();
                         };
                    };
                };
                this.send(groupStanzaReply);
            }
        } else if(stanza.is( Stanza.TYPE.GROUP) && type == Group.TYPE.STANZA_GROUP_ACK){
            var groupStanzas = stanza.getChildElements();
            var totalStanza = groupStanzas.length;

            if(totalStanza > 0){
                var childStanza;
                for(var i = 0; i < totalStanza; i++){
                    if(groupStanzas[i]){
                        try{
                            childStanza = Stanza.prototype.parse(groupStanzas[i])
                        }catch(e){
                            logger.error("Group stanza parsing error",e);
                        };
                        this.emit('stanza',childStanza);
                    };
                };
            }
        }
    }
}

Client.prototype._handleSessionState = function(stanza) {
    if (stanza.attrs.type === 'result') {
        this.state = STATE_AUTHED
        /* jshint camelcase: false */
        this.did_session = true

        /* no stream restart, but next feature (most probably
           we'll go online next) */
       // this.useFeatures()

        this.state = STATE_ONLINE
        this.emit('online', stanza)
    } else {
        this.emit('error', 'CANT_BIND_RESOURSE_HANDLE_SESSION')
    }
}

Client.prototype._handleBindState = function(stanza) {
    if (stanza.attrs.type === 'result') {
        this.state = STATE_AUTHED
        /*jshint camelcase: false */
        this.did_bind = true

        var bindEl = stanza.getChild('bind', Stanza.NAMESPACE.NS_BIND)
        if (bindEl && bindEl.getChild('jid')) {
            this.jid = new JID(bindEl.getChild('jid').getText())
        }

        /* no stream restart, but next feature */
        this.useFeatures()
    } else {
        this.emit('error', 'CANT_BIND_RESOURSE_BIND_SESSION')
    }
}

Client.prototype._handleAuthState = function(stanza) {
    if (stanza.is('challenge', Stanza.NAMESPACE.NS_XMPP_SASL)) {
        var challengeMsg = decode64(stanza.getText())
        var responseMsg = encode64(this.mech.challenge(challengeMsg))
        var response = new Stanza(
            'response', { xmlns: Stanza.NAMESPACE.NS_XMPP_SASL }
        ).t(responseMsg)
        this.send(response)
    } else if (stanza.is('success', Stanza.NAMESPACE.NS_XMPP_SASL)) {
        this.mech = null
        this.state = STATE_AUTHED
        this.emit('auth')
    } else {
        this.emit('error', 'XMPP authentication failure')
    }
}

Client.prototype._handlePreAuthState = function() {
    this.state = STATE_AUTH
    var offeredMechs = this.streamFeatures.
        getChild('mechanisms', Stanza.NAMESPACE.NS_XMPP_SASL).
        getChildren('mechanism', Stanza.NAMESPACE.NS_XMPP_SASL).
        map(function(el) { return el.getText() })
    this.mech = sasl.selectMechanism(
        offeredMechs,
        this.preferredSaslMechanism,
        this.availableSaslMechanisms
    )
    if (this.mech) {
        this.mech.authzid = this.jid.bare().toString()
        this.mech.authcid = this.jid.user
        this.mech.password = this.password
        /*jshint camelcase: false */
        this.mech.api_key = this.api_key
        this.mech.access_token = this.access_token
        this.mech.oauth2_token = this.oauth2_token
        this.mech.oauth2_auth = this.oauth2_auth
        this.mech.realm = this.jid.domain  // anything?
        if (this.actAs) this.mech.actAs = this.actAs.user
        this.mech.digest_uri = 'xmpp/' + this.jid.domain
        var authMsg = encode64(this.mech.auth())
        var attrs = this.mech.authAttrs()
        attrs.xmlns = Stanza.NAMESPACE.NS_XMPP_SASL
        attrs.mechanism = this.mech.name
        this.send(new Stanza('auth', attrs)
            .t(authMsg))
    } else {
        this.emit('error', 'NO_SASL')
    }
}

/**
 * Either we just received <stream:features/>, or we just enabled a
 * feature and are looking for the next.
 */
Client.prototype.useFeatures = function() {
    /* jshint camelcase: false */
    if ((this.state === STATE_PREAUTH) && this.register) {
        delete this.register
        this.doRegister()
    } else if ((this.state === STATE_PREAUTH) &&
        this.streamFeatures.getChild('mechanisms', Stanza.NAMESPACE.NS_XMPP_SASL)) {
        this._handlePreAuthState()
    } else if ((this.state === STATE_AUTHED) &&
               !this.did_bind &&
               this.streamFeatures.getChild('bind', Stanza.NAMESPACE.NS_BIND)) {
        this.state = STATE_BIND
        var bindEl = new Stanza(
            'iq',
            { type: 'set', id: IQID_BIND }
        ).c('bind', { xmlns: Stanza.NAMESPACE.NS_BIND })
        if (this.jid.resource)
            bindEl.c('resource').t(this.jid.resource)
        this.send(bindEl)
    } else if ((this.state === STATE_AUTHED) &&
               !this.did_session &&
               this.streamFeatures.getChild('session', Stanza.NAMESPACE.NS_SESSION)) {
        this.state = STATE_SESSION
        var stanza = new Stanza(
          'iq',
          { type: 'set', to: this.jid.domain, id: IQID_SESSION  }
        ).c('session', { xmlns: Stanza.NAMESPACE.NS_SESSION })
        this.send(stanza)
    } else if (this.state === STATE_AUTHED) {
        /* Ok, we're authenticated and all features have been
           processed */
        /*this.state = STATE_ONLINE
        this.emit('online', { jid: this.jid })*/
    }
}

Client.prototype.doRegister = function() {
    var id = 'register' + Math.ceil(Math.random() * 99999);
    var isForce = this.options.force || false;
    var iq = new Stanza(
        'iq',
        { type: 'set', id: id, to: this.jid.domain }
    ).c('query', { xmlns: Stanza.NAMESPACE.NS_REGISTER , 'force' : isForce })
    .c('username').t(this.jid.user).up()
    .c('password').t(this.password)
    this.send(iq)

    var self = this
    var onReply = function(reply) {
        if (reply.is('iq') && (reply.attrs.id === id)) {
            self.removeListener('stanza', onReply);

            if (reply.attrs.type === 'result') {
                /* Registration successful, proceed to auth */
                self.useFeatures()
            } else if(reply.attrs.type === 'error'){
                var err = reply.getChild('error');
                if(err){
                    var errCondition = err.getChildByAttr('xmlns',Stanza.NAMESPACE.NS_STANZAS).name;
                    var errCode = err.getAttr('code');
                    var errType = err.getAttr('type');
                    var errMsg = err.getAttr('message');
                    var error = {
                        'id': id,
                        condition : errCondition,
                        code: errCode,
                        type: errType,
                        message: errMsg
                    }
                   self.emit('error', error);
                }
            }
        }
    }
    this.on('stanza:preauth', onReply)
}

/**
 * returns all registered sasl mechanisms
 */
Client.prototype.getSaslMechanisms = function() {
    return this.availableSaslMechanisms
}

/**
 * removes all registered sasl mechanisms
 */
Client.prototype.clearSaslMechanism = function() {
    this.availableSaslMechanisms = []
}

/**
 * register a new sasl mechanism
 */
Client.prototype.registerSaslMechanism = function(method) {
    // check if method is registered
    if (this.availableSaslMechanisms.indexOf(method) === -1 ) {
        this.availableSaslMechanisms.push(method)
    }
}

/**
 * unregister an existing sasl mechanism
 */
Client.prototype.unregisterSaslMechanism = function(method) {
    // check if method is registered
    var index = this.availableSaslMechanisms.indexOf(method)
    if (index >= 0) {
        this.availableSaslMechanisms = this.availableSaslMechanisms.splice(index, 1)
    }
}

Client.SASL = sasl
Client.JID = JID
Client.Element = Element
Client.Message = Message
Client.Stanza = Stanza
Client.Iq = Iq
Client.Group = Group
Client.Presence = Presence


module.exports = Client

},{"./lib/authentication/anonymous":2,"./lib/authentication/external":3,"./lib/authentication/plain":5,"./lib/authentication/xoauth2":6,"./lib/base64":7,"./lib/element":8,"./lib/jid":9,"./lib/sasl":10,"./lib/session":11,"./lib/util":12}],2:[function(require,module,exports){
'use strict';

var util = require('../util')
  , Mechanism = require('./mechanism')

/**
 * @see http://tools.ietf.org/html/rfc4505
 * @see http://xmpp.org/extensions/xep-0175.html
 */
function Anonymous() {}

util.inherits(Anonymous, Mechanism)

Anonymous.prototype.name = 'ANONYMOUS'

Anonymous.prototype.auth = function() {
    return this.authzid
};

Anonymous.prototype.match = function() {
    return true
}

module.exports = Anonymous

},{"../util":12,"./mechanism":4}],3:[function(require,module,exports){
'use strict';

var util = require('../util')
  , Mechanism = require('./mechanism')

/**
 * @see http://xmpp.org/extensions/xep-0178.html
 */
function External() {}

util.inherits(External, Mechanism)

External.prototype.name = 'EXTERNAL'

External.prototype.auth = function() {
    return (this.authzid)
}

External.prototype.match = function(options) {
    if (options.credentials) return true
    return false
}

module.exports = External

},{"../util":12,"./mechanism":4}],4:[function(require,module,exports){
'use strict';

/**
 * Each implemented mechanism offers multiple methods
 * - name : name of the auth method
 * - auth :
 * - match: checks if the client has enough options to
 *          offer this mechanis to xmpp servers
 * - authServer: takes a stanza and extracts the information
 */

var util = require('../util')
  , EventEmitter = require('events').EventEmitter

// Mechanisms
function Mechanism() {}

util.inherits(Mechanism, EventEmitter)

Mechanism.prototype.authAttrs = function() {
    return {}
}

module.exports = Mechanism

},{"../util":12,"events":15}],5:[function(require,module,exports){
'use strict';

var util = require('../util')
  , Mechanism = require('./mechanism')

function Plain() {}

util.inherits(Plain, Mechanism)

Plain.prototype.name = 'PLAIN'

Plain.prototype.auth = function() {
    return this.authzid + '\0' +
        this.authcid + '\0' +
        this.password;
}

Plain.prototype.match = function(options) {
    if (options.password) return true
    return false
}

module.exports = Plain

},{"../util":12,"./mechanism":4}],6:[function(require,module,exports){
'use strict';

var util = require('../util')
  , Mechanism = require('./mechanism')

/**
 * @see https://developers.google.com/talk/jep_extensions/oauth
 */
/*jshint camelcase: false */
function XOAuth2() {
    this.oauth2_auth = null
    this.authzid = null
}

util.inherits(XOAuth2, Mechanism)

XOAuth2.prototype.name = 'X-OAUTH2'
XOAuth2.prototype.NS_GOOGLE_AUTH = 'http://www.google.com/talk/protocol/auth'

XOAuth2.prototype.auth = function() {
    return '\0' + this.authzid + '\0' + this.oauth2_token
}

XOAuth2.prototype.authAttrs = function() {
    return {
        'auth:service': 'oauth2',
        'xmlns:auth': this.oauth2_auth
    }
}

XOAuth2.prototype.match = function(options) {
    return (options.oauth2_auth === XOAuth2.prototype.NS_GOOGLE_AUTH)
}

module.exports = XOAuth2

},{"../util":12,"./mechanism":4}],7:[function(require,module,exports){
var Base64 = {
    // public method for encoding
    encode : function (input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;

        input = _utf8_encode(input);

        while (i < input.length) {

            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);

            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }

            output = output +
            _keyStr.charAt(enc1) + _keyStr.charAt(enc2) +
            _keyStr.charAt(enc3) + _keyStr.charAt(enc4);

        }

        return output;
    },

    // public method for decoding
    decode : function (input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;

        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

        while (i < input.length) {

            enc1 = _keyStr.indexOf(input.charAt(i++));
            enc2 = _keyStr.indexOf(input.charAt(i++));
            enc3 = _keyStr.indexOf(input.charAt(i++));
            enc4 = _keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }

        }

        output = _utf8_decode(output);

        return output;

    }
};

// private property

var  _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

// private method for UTF-8 encoding
function _utf8_encode(string) {
	string = string.replace(/\r\n/g,"\n");
	var utftext = "";

	for (var n = 0; n < string.length; n++) {

		var c = string.charCodeAt(n);

		if (c < 128) {
			utftext += String.fromCharCode(c);
		}
		else if((c > 127) && (c < 2048)) {
			utftext += String.fromCharCode((c >> 6) | 192);
			utftext += String.fromCharCode((c & 63) | 128);
		}
		else {
			utftext += String.fromCharCode((c >> 12) | 224);
			utftext += String.fromCharCode(((c >> 6) & 63) | 128);
			utftext += String.fromCharCode((c & 63) | 128);
		}

	}

	return utftext;
}

// private method for UTF-8 decoding
function _utf8_decode(utftext) {
	var string = "";
	var i = 0;
	var c = c1 = c2 = 0;

	while ( i < utftext.length ) {

		c = utftext.charCodeAt(i);

		if (c < 128) {
			string += String.fromCharCode(c);
			i++;
		}
		else if((c > 191) && (c < 224)) {
			c2 = utftext.charCodeAt(i+1);
			string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
			i += 2;
		}
		else {
			c2 = utftext.charCodeAt(i+1);
			c3 = utftext.charCodeAt(i+2);
			string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
			i += 3;
		}

	}

	return string;
}

exports.Base64 = Base64

},{}],8:[function(require,module,exports){
'use strict';

var util = require('./util');

/**
 * Attributes are in the element.attrs object. Children is a list of
 * either other Elements or Strings for text content.
 **/

function Element(name, attrs) {
    this.name = name
    this.parent = null
    this.attrs = attrs || {}
    this.children = []
}

/*** Accessors ***/

/**
 * if (element.is('message', 'jabber:client')) ...
 **/

Element.prototype.is = function(name, xmlns) {
    return (this.getName() === name) &&
        (!xmlns || (this.getNS() === xmlns))
}

/* without prefix */
Element.prototype.getName = function() {
    if (this.name.indexOf(':') >= 0)
        return this.name.substr(this.name.indexOf(':') + 1)
    else
        return this.name
}

/**
 * retrieves the namespace of the current element, upwards recursively
 **/
Element.prototype.getNS = function() {
    if (this.name.indexOf(':') >= 0) {
        var prefix = this.name.substr(0, this.name.indexOf(':'))
        return this.findNS(prefix)
    } else {
        return this.findNS()
    }
}

/**
 * find the namespace to the given prefix, upwards recursively
 **/
Element.prototype.findNS = function(prefix) {
    if (!prefix) {
        /* default namespace */
        if (this.attrs.xmlns)
            return this.attrs.xmlns
        else if (this.parent)
            return this.parent.findNS()
    } else {
        /* prefixed namespace */
        var attr = 'xmlns:' + prefix
        if (this.attrs[attr])
            return this.attrs[attr]
        else if (this.parent)
            return this.parent.findNS(prefix)
    }
}

/**
 * Recursiverly gets all xmlns defined, in the form of {url:prefix}
 **/
Element.prototype.getXmlns = function() {
    var namespaces = {}

    if (this.parent)
        namespaces = this.parent.getXmlns()

    for (var attr in this.attrs) {
        var m = attr.match('xmlns:?(.*)')
        if (this.attrs.hasOwnProperty(attr) && m) {
            namespaces[this.attrs[attr]] = m[1]
        }
    }
    return namespaces
}


/**
 * xmlns can be null, returns the matching attribute.
 **/
Element.prototype.getAttr = function(name, xmlns) {
    if (!xmlns)
        return this.attrs[name]

    var namespaces = this.getXmlns()

    if (!namespaces[xmlns])
        return null

    return this.attrs[[namespaces[xmlns], name].join(':')]
}

/**
 * xmlns can be null
 **/
Element.prototype.getChild = function(name, xmlns) {
    return this.getChildren(name, xmlns)[0]
}

/**
 * xmlns can be null
 **/
Element.prototype.getChildren = function(name, xmlns) {
    var result = []
    for (var i = 0; i < this.children.length; i++) {
        var child = this.children[i]
        if (child.getName &&
            (child.getName() === name) &&
            (!xmlns || (child.getNS() === xmlns)))
            result.push(child)
    }
    return result
}

/**
 * xmlns and recursive can be null
 **/
Element.prototype.getChildByAttr = function(attr, val, xmlns, recursive) {
    return this.getChildrenByAttr(attr, val, xmlns, recursive)[0]
}

/**
 * xmlns and recursive can be null
 **/
Element.prototype.getChildrenByAttr = function(attr, val, xmlns, recursive) {
    var result = []
    for (var i = 0; i < this.children.length; i++) {
        var child = this.children[i]
        if (child.attrs &&
            (child.attrs[attr] === val) &&
            (!xmlns || (child.getNS() === xmlns)))
            result.push(child)
        if (recursive && child.getChildrenByAttr) {
            result.push(child.getChildrenByAttr(attr, val, xmlns, true))
        }
    }
    if (recursive) result = [].concat.apply([], result)
    return result
}

Element.prototype.getChildrenByFilter = function(filter, recursive) {
    var result = []
    for (var i = 0; i < this.children.length; i++) {
        var child = this.children[i]
        if (filter(child))
            result.push(child)
        if (recursive && child.getChildrenByFilter){
            result.push(child.getChildrenByFilter(filter, true))
        }
    }
    if (recursive) {
        result = [].concat.apply([], result)
    }
    return result
}

Element.prototype.getText = function() {
    var text = ''
    for (var i = 0; i < this.children.length; i++) {
        var child = this.children[i]
        if ((child.constructor == String) || (typeof child === 'number')) {
            text += child
        }
    }
    return text
}

Element.prototype.getChildText = function(name, xmlns) {
    var child = this.getChild(name, xmlns)
    return child ? child.getText() : null
}

/**
 * Return all direct descendents that are Elements.
 * This differs from `getChildren` in that it will exclude text nodes,
 * processing instructions, etc.
 */
Element.prototype.getChildElements = function() {
    return this.getChildrenByFilter(function(child) {
        return child instanceof Element
    })
}

/*** Builder ***/

/** returns uppermost parent */
Element.prototype.root = function() {
    if (this.parent)
        return this.parent.root()
    else
        return this
}

Element.prototype.tree = Element.prototype.root

/** just parent or itself */
Element.prototype.up = function() {
    if (this.parent)
        return this.parent
    else
        return this
}

Element.prototype._getElement = function(name, attrs) {
    var element = new Element(name, attrs)
    return element
}

/** create child node and return it */
Element.prototype.c = function(name, attrs) {
    return this.cnode(this._getElement(name, attrs))
}

Element.prototype.cnode = function(child) {
    this.children.push(child)
    child.parent = this
    return child
}

/** add text node and return element */
Element.prototype.t = function(text) {
    this.children.push(text)
    return this
}

/*** Manipulation ***/

/**
 * Either:
 *   el.remove(childEl)
 *   el.remove('author', 'urn:...')
 */
Element.prototype.remove = function(el, xmlns) {
    var filter
    if (typeof el === 'string') {
        /* 1st parameter is tag name */
        filter = function(child) {
            return !(child.is &&
                 child.is(el, xmlns))
        }
    } else {
        /* 1st parameter is element */
        filter = function(child) {
            return child !== el
        }
    }
    this.children = this.children.filter(filter)
    return this
}

/**
 * To use in case you want the same XML data for separate uses.
 * Please refrain from this practise unless you know what you are
 * doing. Building XML with ltx is easy!
 */
Element.prototype.clone = function() {
    var clone = this._getElement(this.name, {})
    for (var k in this.attrs) {
        if (this.attrs.hasOwnProperty(k))
            clone.attrs[k] = this.attrs[k]
    }
    for (var i = 0; i < this.children.length; i++) {
        var child = this.children[i]
        clone.cnode(child.clone ? child.clone() : child)
    }
    return clone
}

Element.prototype.text = function(val) {
    if (val && this.children.length === 1) {
        this.children[0] = val
        return this
    }
    return this.getText()
}

Element.prototype.attr = Element.prototype.setAttr = function(attr, val) {
    if (((typeof val !== 'undefined') || (val === null))) {
        if (!this.attrs) {
            this.attrs = {}
        }
        this.attrs[attr] = val
        return this
    }
    return this.attrs[attr]
}

/*** Serialization ***/

Element.prototype.toString = function() {
    return JSON.stringify(this.toJSON());
}

Element.prototype.toJSON = function() {
    return {
        name: this.name,
        attrs: this.attrs,
        children: this.children.map(function(child) {
            return child && child.toJSON ? child.toJSON() : child;
        })
    }
}

Element.prototype.write = function(writer) {
    writer(this.toJSON());
}

/*
 TO-DO: Write testcase for it
*/

var parseXMLElement = function(jsonXMl) {
    var element;
	if(jsonXMl.constructor == String){
		 element = new String(jsonXMl);
	} else if(jsonXMl.name){
		element = new Element(jsonXMl.name, jsonXMl.attrs);
		if(jsonXMl.children && jsonXMl.children.length > 0){
		    var length = jsonXMl.children.length;
			for (var i = 0; i < length; i++) {
			    var child = jsonXMl.children[i];
				if(child){
				    var elm  = parseXMLElement(child);
	                elm && element.cnode(elm);
				}
			}
		}
	}
    return element;
};

Element.prototype.parse = parseXMLElement

function Stanza(name, attrs) {
    Element.call(this, name, attrs)
}

Stanza.TYPE = {
    MESSAGE: 'message',
    IQ: 'iq',
    PRESENCE: 'presence',
    GROUP: 'group'
}

util.inherits(Stanza, Element)

/**
 * Common attribute getters/setters for all stanzas
 */

Object.defineProperty(Stanza.prototype, 'from', {
    get: function() {
        return this.attrs.from
    },

    set: function(from) {
        this.attrs.from = from
    }
});

Object.defineProperty(Stanza.prototype, 'to', {
    get: function() {
        return this.attrs.to
    },

    set: function(to) {
        this.attrs.to = to
    }
});

Object.defineProperty(Stanza.prototype, 'id', {
    get: function() {
        return this.attrs.id
    },

    set: function(id) {
        this.attrs.id = id
    }
});

Object.defineProperty(Stanza.prototype, 'type', {
    get: function() {
        return this.attrs.type
    },

    set: function(type) {
        this.attrs.type = type
    }
});

var stanzaNumber = 0;
Stanza.prototype.generateID = function (prefix){
    prefix = prefix || '';
    return prefix+'_'+(Math.random()*0xffffffff)+'_'+Date.now()+'_'+(stanzaNumber++);
};

/**
 * Stanza kinds
 */

function Message(attrs) {
    Stanza.call(this, Stanza.TYPE.MESSAGE, attrs)
}

util.inherits(Message, Stanza);

Message.TYPE = {
    SERVER_RECEIVE_ACK : 'srvr_rcv_ack',
    USER_RECEIVE_ACK : 'usr_rcv_ack',
    MESSAGE: 'msg'
}

Message.create = function(opts){
    opts.type = opts.type || Message.TYPE.MESSAGE;
    opts.created = opts.created || Date.now();
    opts.id = opts.id || Stanza.prototype.generateID('clnt_msgcrt');
    var message = new Message({
        from: opts.from,
        to: opts.to,
        id: opts.id,
        'type': opts.type,
        'created': opts.created
    }).c('body',{}).t(opts.text);
    opts.ackid && message.root().attr('ackid',opts.ackid);
    return message;
};


function Presence(attrs) {
    Stanza.call(this, Stanza.TYPE.PRESENCE, attrs)
}

util.inherits(Presence, Stanza)

Presence.create = function(opts){
    opts.created = opts.created || Date.now();
    opts.id = opts.id || Stanza.prototype.generateID('clnt_prsnccrt');
    var presence = new Presence({
        id: opts.id,
        'created': opts.created
    });
    opts.from && presence.attr('from',opts.from);
    opts.to && presence.attr('to',opts.to);
    opts.type && presence.attr('type',opts.type);
    opts.status && presence.attr('status',opts.status);
    opts.lastOnline && presence.attr('lastOnline',opts.lastOnline);
    return presence;
};

/*
     DEFAULT type is Available(When not specified)
     <xs:attribute name='type' use='optional'>
        <xs:simpleType>
          <xs:restriction base='xs:NCName'>
            <xs:enumeration value='error'/>
            <xs:enumeration value='probe'/>
            <xs:enumeration value='subscribe'/>
            <xs:enumeration value='subscribed'/>
            <xs:enumeration value='unavailable'/>
            <xs:enumeration value='unsubscribe'/>
            <xs:enumeration value='unsubscribed'/>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
*/

Presence.TYPE = {
    UNAVAILABLE: 'unavailable',
    CONTACTSPRESENCE: 'contactspresence',
    SUBSCRIBE: 'subscribe',
    SUBSCRIBED: 'subscribed',
    UNSUBSCRIBE: 'unsubscribe',
    UNSUBSCRIBED: 'unsubscribed',
    PROBE: 'probe',
    ERROR: 'error',
};

Presence.STATUS = {
    ONLINE: 'online',
    OFFLINE: 'offline',
    AWAY: 'away'
};

function Iq(attrs) {
    Stanza.call(this, Stanza.TYPE.IQ, attrs)
}

Iq.TYPE = {
    RESULT: 'result',
    GET: 'get',
    SET: 'set',
    ERROR: 'error'
};


Iq.create = function(opts){
    var stanzaId = opts.id || Iq.prototype.generateID('srvr_iqcrt');
    opts.created = opts.created || Date.now();
    opts.queryType = opts.queryType;

    var iqStanza = new Iq({
            type: opts.type || 'result',
            id: stanzaId , 'to': opts.to ,
            'created': opts.created
        }).c('query',{ 'xmlns':opts.queryType});

    return iqStanza; // It stanza new create but pointer at query node
};

Iq.NAMESPACE = {
    PUSH_NOTIFY: 'iq:notify:push',
    APPLICATION: 'iq:application',
    PING:'urn:xmpp:ping'
};

util.inherits(Iq, Stanza);

function Group(attrs) {
    Stanza.call(this, Stanza.TYPE.GROUP, attrs)
}

Group.TYPE = {
    STANZA_GROUP: 'msg_grp',
    STANZA_GROUP_ACK: 'msg_grp_usr_ack'
}

util.inherits(Group, Stanza);

Group.create = function(opts){
    opts.type = opts.type || Group.TYPE.STANZA_GROUP;
    opts.created = opts.created || Date.now();
    opts.id = opts.id || Stanza.prototype.generateID('clnt_grpcrt');
    var grp = new Group({
        to: opts.to,
        id: opts.id,
        'created': opts.created,
        'type': opts.type
    });
    opts.ackid && grp.attr('ackid',opts.ackid);
    return grp;
}

Stanza.NAMESPACE = {

    NS_CLIENT: 'jabber:client',
    NS_XMPP_SASL: 'urn:ietf:params:xml:ns:xmpp-sasl',
    NS_REGISTER: 'jabber:iq:register',
    NS_SESSION: 'urn:ietf:params:xml:ns:xmpp-session',
    NS_BIND: 'urn:ietf:params:xml:ns:xmpp-bind',
    NS_STANZAS: 'urn:ietf:params:xml:ns:xmpp-stanzas',
    NS_XMPP_TLS: 'urn:ietf:params:xml:ns:xmpp-tls',
    NS_STREAM: 'http://etherx.jabber.org/streams',
    NS_XMPP_STREAMS: 'urn:ietf:params:xml:ns:xmpp-streams',
    NAME_STREAM: 'stream:stream'

}

/*
     More Info: http://xmpp.org/extensions/xep-0086.html
     Erro code:

      <error type='modify'>
        <bad-request xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>
      </error>

            <bad-request/>	modify	400
            <conflict/>	cancel	409
    <feature-not-implemented/>	cancel	501
    <forbidden/>	auth	403
    <gone/>	modify	302 (permanent)
            <internal-server-error/>	wait	500
            <item-not-found/>	cancel	404
    <jid-malformed/>	modify	400
            <not-acceptable/>	modify	406
    <not-allowed/>	cancel	405
    <not-authorized/>	auth	401
    <payment-required/>	auth	402
    <recipient-unavailable/>	wait	404
    <redirect/>	modify	302 (temporary)
    <registration-required/>	auth	407
    <remote-server-not-found/>	cancel	404
    <remote-server-timeout/>	wait	504
    <resource-constraint/>	wait	500
    <service-unavailable/>	cancel	503
    <subscription-required/>	auth	407
    <undefined-condition/>	[any]	500
    <unexpected-request/>	wait	400

*/

Stanza.ERROR = {
    BAD_REQUEST: {
        'type': 'modify',
        'code': '400',
        'condition': 'bad-request'
    },

    CONFLICT: {
        'type': 'cancel',
        'code': '409',
        'condition': 'conflict'
    },

    NOT_ACCEPTABLE: {
        'type': 'modify',
        'code': '406',
        'condition': 'not-acceptable'
    },

    NOT_FOUND: {
        'type': 'cancel',
        'code': '404',
        'condition': 'item-not-found'
    },

    INTERNAL_SERVER_ERROR: {
        'type': 'wait',
        'code': '500',
        'condition': 'internal-server-error'
    }
};

Stanza.prototype.addError = function(err){
    if(this.root){
        this.root();
    };
    this.attrs.type = 'error';
    this.c('error', {
            code: '' + err.code,
            type: err.type,
            message: err.message || err.text
        })
        .c(err.condition || '' , {
            xmlns: Stanza.NAMESPACE.NS_STANZAS
        });
};


exports.Element = Element
exports.Stanza = Stanza
exports.Message = Message
exports.Presence = Presence
exports.Iq = Iq
exports.Group = Group

},{"./util":12}],9:[function(require,module,exports){
/**
 * JID implements
 * - Xmpp addresses according to RFC6122
 * - XEP-0106: JID Escaping
 *
 * @see http://tools.ietf.org/html/rfc6122#section-2
 * @see http://xmpp.org/extensions/xep-0106.html

  //   admin@stanformc.com/bio   admin is local/username
 */
function JID(a, b, c) {
    this.local = null
    this.domain = null
    this.resource = null

    if (a && (!b) && (!c)) {
        this.parse(a)
    } else if (b) {
        this.setLocal(a)
        this.setDomain(b)
        this.setResource(c)
    } else {
        throw new Error('Argument error')
    }
}

JID.prototype.parse = function(s) {
    if (s.indexOf('@') >= 0) {
        this.setLocal(s.substr(0, s.lastIndexOf('@')))
        s = s.substr(s.lastIndexOf('@') + 1)
    }
    if (s.indexOf('/') >= 0) {
        this.setResource(s.substr(s.indexOf('/') + 1))
        s = s.substr(0, s.indexOf('/'))
    }
    this.setDomain(s)
}

JID.prototype.toString = function(unescape) {
    var s = this.domain
    if (this.local) s = this.getLocal(unescape) + '@' + s
    if (this.resource) s = s + '/' + this.resource
    return s
}

/**
 * Convenience method to distinguish users
 **/
JID.prototype.bare = function() {
    if (this.resource) {
        return new JID(this.local, this.domain, null)
    } else {
        return this
    }
}

/**
 * Comparison function
 **/
JID.prototype.equals = function(other) {
    return (this.local === other.local) &&
        (this.domain === other.domain) &&
        (this.resource === other.resource)
}

/* Deprecated, use setLocal() [see RFC6122] */
JID.prototype.setUser = function(user) {
    return this.setLocal(user)
}

/**
 * Setters that do stringprep normalization.
 **/
JID.prototype.setLocal = function(local, escape) {
    escape = escape || this.detectEscape(local)
    if (escape) {
        local = this.escapeLocal(local)
    }
    this.local = this.user = local;
    return this
}

/**
 * http://xmpp.org/rfcs/rfc6122.html#addressing-domain
 */
JID.prototype.setDomain = function(domain) {
    this.domain = domain;
    return this
}

JID.prototype.setResource = function(resource) {
    this.resource = resource;
    return this
}

JID.prototype.getLocal = function(unescape) {
    unescape = unescape || false
    var local = null

    if (unescape) {
        local = this.unescapeLocal(this.local)
    } else {
        local = this.local
    }

    return local;
}


/* Deprecated, use getLocal() [see RFC6122] */
JID.prototype.getUser = function() {
    return this.getLocal()
}

JID.prototype.getDomain = function() {
    return this.domain
}

JID.prototype.getResource = function() {
    return this.resource
}

JID.prototype.detectEscape = function (local) {
    if (!local) return false

    // remove all escaped secquences
    var tmp = local.replace(/\\20/g, '')
        .replace(/\\22/g, '')
        .replace(/\\26/g, '')
        .replace(/\\27/g, '')
        .replace(/\\2f/g, '')
        .replace(/\\3a/g, '')
        .replace(/\\3c/g, '')
        .replace(/\\3e/g, '')
        .replace(/\\40/g, '')
        .replace(/\\5c/g, '')

    // detect if we have unescaped sequences
    var search = tmp.search(/\\| |\"|\&|\'|\/|:|<|>|@/g);
    if (search === -1) {
        return false
    } else {
        return true
    }
}

/**
 * Escape the local part of a JID.
 *
 * @see http://xmpp.org/extensions/xep-0106.html
 * @param String local local part of a jid
 * @return An escaped local part
 */
JID.prototype.escapeLocal = function (local) {
    if (local === null) return null

    /* jshint -W044 */
    return local.replace(/^\s+|\s+$/g, '')
        .replace(/\\/g, '\\5c')
        .replace(/ /g, '\\20')
        .replace(/\"/g, '\\22')
        .replace(/\&/g, '\\26')
        .replace(/\'/g, '\\27')
        .replace(/\//g, '\\2f')
        .replace(/:/g, '\\3a')
        .replace(/</g, '\\3c')
        .replace(/>/g, '\\3e')
        .replace(/@/g, '\\40')
        .replace(/\3a/g, '\5c3a')
}

/**
 * Unescape a local part of a JID.
 *
 * @see http://xmpp.org/extensions/xep-0106.html
 * @param String local local part of a jid
 * @return unescaped local part
 */
JID.prototype.unescapeLocal = function (local) {
    if (local === null) return null

    return local.replace(/\\20/g, ' ')
        .replace(/\\22/g, '\"')
        .replace(/\\26/g, '&')
        .replace(/\\27/g, '\'')
        .replace(/\\2f/g, '/')
        .replace(/\\3a/g, ':')
        .replace(/\\3c/g, '<')
        .replace(/\\3e/g, '>')
        .replace(/\\40/g, '@')
        .replace(/\\5c/g, '\\')
}

module.exports = JID

},{}],10:[function(require,module,exports){
'use strict';

var Mechanism = require('./authentication/mechanism')

/**
 * Available methods for client-side authentication (Client)
 * @param  Array offeredMechs  methods offered by server
 * @param  Array preferredMech preferred methods by client
 * @param  Array availableMech available methods on client
 */
function selectMechanism(offeredMechs, preferredMech, availableMech) {
    var mechClasses = []
    var byName = {}
    var Mech
    if (Array.isArray(availableMech)) {
        mechClasses = mechClasses.concat(availableMech)
    }
    mechClasses.forEach(function(mechClass) {
        byName[mechClass.prototype.name] = mechClass
    })
    /* Any preferred? */
    if (byName[preferredMech] &&
        (offeredMechs.indexOf(preferredMech) >= 0)) {
        Mech = byName[preferredMech]
    }
    /* By priority */
    mechClasses.forEach(function(mechClass) {
        if (!Mech &&
            (offeredMechs.indexOf(mechClass.prototype.name) >= 0))
            Mech = mechClass
    })

    return Mech ? new Mech() : null
}

/**
 * Will detect the available mechanisms based on the given options
 * @param  {[type]} options client configuration
 * @param  Array availableMech available methods on client
 * @return {[type]}         available options
 */
function detectMechanisms(options, availableMech) {
    var mechClasses = availableMech ? availableMech : []

    var detect = []
    mechClasses.forEach(function(mechClass) {
        var match = mechClass.prototype.match
        if (match(options)) detect.push(mechClass)
    })
    return detect
}

exports.selectMechanism = selectMechanism
exports.detectMechanisms = detectMechanisms
exports.AbstractMechanism = Mechanism

},{"./authentication/mechanism":4}],11:[function(require,module,exports){
'use strict';

var util = require('./util')
  , EventEmitter = require('events').EventEmitter
  , JID = require('./jid.js')
  , WSConnection = require('./websockets')

function Session(opts) {
    EventEmitter.call(this)

    this.setOptions(opts)

    if (opts.websocket && opts.websocket.url) {
        this._setupWebsocketConnection(opts)
    }
}

util.inherits(Session, EventEmitter)

Session.prototype._setupWebsocketConnection = function(opts) {
    this.connection = new WSConnection({
        jid: this.jid,
        websocket: opts.websocket
    })
    this._addConnectionListeners(this.connection)
    this.connection.on('connected', function() {
        // Clients start <stream:stream>, servers reply
        if (this.connection.startStream)
            this.connection.startStream()
    }.bind(this))
}

Session.prototype.setOptions = function(opts) {
    /* jshint camelcase: false */
    this.jid = (typeof opts.jid === 'string') ? new JID(opts.jid) : opts.jid
    this.password = opts.password
    this.preferredSaslMechanism = opts.preferredSaslMechanism
    this.api_key = opts.api_key
    this.access_token = opts.access_token
    this.oauth2_token = opts.oauth2_token
    this.oauth2_auth = opts.oauth2_auth
    this.register = opts.register
    this.wait = opts.wait || '10'
    if (typeof opts.actAs === 'string') {
        this.actAs = new JID(opts.actAs)
    } else {
        this.actAs = opts.actAs
    }
}

Session.prototype._addConnectionListeners = function (con) {
    con = con || this.connection
    con.on('stanza', this.onStanza.bind(this))
    con.on('drain', this.emit.bind(this, 'drain'))
    con.on('end', this.emit.bind(this, 'end'))
    con.on('close', this.emit.bind(this, 'close'))
    con.on('error', this.emit.bind(this, 'error'))
    con.on('connect', this.emit.bind(this, 'connect'))
    con.on('reconnect', this.emit.bind(this, 'reconnect'))
    con.on('disconnect', this.emit.bind(this, 'disconnect'))
    if (con.startStream) {
        con.on('connect', function () {
            // Clients start <stream:stream>, servers reply
            con.startStream()
        })
        this.on('auth', function () {
            con.startStream()
        })
    }
}

Session.prototype.pause = function() {
    if (this.connection && this.connection.pause)
        this.connection.pause()
}

Session.prototype.resume = function() {
    if (this.connection && this.connection.resume)
        this.connection.resume()
}

Session.prototype.send = function(stanza) {
    return this.connection ? this.connection.send(stanza) : false
}

Session.prototype.end = function() {
    if (this.connection)
        this.connection.end()
}

module.exports = Session

},{"./jid.js":9,"./util":12,"./websockets":13,"events":15}],12:[function(require,module,exports){
/**
 * Inherit the prototype methods from one constructor into another.
 *
 * The Function.prototype.inherits from lang.js rewritten as a standalone
 * function (not on Function.prototype). NOTE: If this file is to be loaded
 * during bootstrapping this function needs to be rewritten using some native
 * functions as prototype setup using normal JavaScript does not work as
 * expected during bootstrapping (see mirror.js in r114903).
 *
 * @param {function} ctor Constructor function which needs to inherit the
 *     prototype.
 * @param {function} superCtor Constructor function to inherit prototype from.
 */
exports.inherits = function(ctor, superCtor) {
  ctor.super_ = superCtor;
  ctor.prototype = Object.create(superCtor.prototype, {
    constructor: {
      value: ctor,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
};

exports._extend = function(origin, add) {
  // Don't do anything if add isn't an object
  if (!add || !isObject(add)) return origin;

  var keys = Object.keys(add);
  var i = keys.length;
  while (i--) {
    origin[keys[i]] = add[keys[i]];
  }
  return origin;
};

function hasOwnProperty(obj, prop) {
  return Object.prototype.hasOwnProperty.call(obj, prop);
}


// NOTE: These type checking functions intentionally don't use `instanceof`
// because it is fragile and can be easily faked with `Object.create()`.
var isArray = exports.isArray = Array.isArray;

function isBoolean(arg) {
  return typeof arg === 'boolean';
}
exports.isBoolean = isBoolean;

function isNull(arg) {
  return arg === null;
}
exports.isNull = isNull;

function isNullOrUndefined(arg) {
  return arg == null;
}
exports.isNullOrUndefined = isNullOrUndefined;

function isNumber(arg) {
  return typeof arg === 'number';
}
exports.isNumber = isNumber;

function isString(arg) {
  return typeof arg === 'string';
}
exports.isString = isString;

function isSymbol(arg) {
  return typeof arg === 'symbol';
}
exports.isSymbol = isSymbol;

function isUndefined(arg) {
  return arg === void 0;
}
exports.isUndefined = isUndefined;

function isRegExp(re) {
  return isObject(re) && objectToString(re) === '[object RegExp]';
}
exports.isRegExp = isRegExp;

function isObject(arg) {
  return typeof arg === 'object' && arg !== null;
}
exports.isObject = isObject;

function isDate(d) {
  return isObject(d) && objectToString(d) === '[object Date]';
}
exports.isDate = isDate;

function isError(e) {
  return isObject(e) &&
      (objectToString(e) === '[object Error]' || e instanceof Error);
}
exports.isError = isError;

function isFunction(arg) {
  return typeof arg === 'function';
}
exports.isFunction = isFunction;

function isPrimitive(arg) {
  return arg === null ||
         typeof arg === 'boolean' ||
         typeof arg === 'number' ||
         typeof arg === 'string' ||
         typeof arg === 'symbol' ||  // ES6 symbol
         typeof arg === 'undefined';
}
exports.isPrimitive = isPrimitive;

function objectToString(o) {
  return Object.prototype.toString.call(o);
}

var consoleLogger = {};

consoleLogger.log = function(msg,obj) {
  console.log(Date.now()+' : '+msg,obj || '');
}

consoleLogger.error = function(msg,err) {
  var error = err || {};
  console.error(Date.now()+' : '+msg+ ' :: ' +error.message);
  error.stack && console.error(error.stack);
}

var gcLogger = consoleLogger;

exports.logger = gcLogger;



},{}],13:[function(require,module,exports){
'use strict';

var EventEmitter = require('events').EventEmitter
	, WebSocket = require('faye-websocket') && require('faye-websocket').Client ?
      require('faye-websocket').Client : window.WebSocket
	, util = require('./util')
  	, Element	= require('./element.js').Element

var logger =  util.logger;

function WSConnection(opts) {
    EventEmitter.call(this)

    this.url = opts.websocket.url
    this.jid = opts.jid
    this.xmlns = {}

    this.websocket = new WebSocket(this.url)
	/*
	Earlier code was like this
	this.websocket = new WebSocket(this.url, ['xmpp'])
	*/
    this.websocket.onopen = this.onopen.bind(this)
    this.websocket.onmessage = this.onmessage.bind(this)
    this.websocket.onclose = this.onclose.bind(this)
    this.websocket.onerror = this.onerror.bind(this)
}

util.inherits(WSConnection, EventEmitter)

WSConnection.prototype.maxStanzaSize = 65535
WSConnection.prototype.xmppVersion = '1.0'

var NS_XMPP_TLS = 'urn:ietf:params:xml:ns:xmpp-tls'
var NS_STREAM = 'http://etherx.jabber.org/streams'
var NS_XMPP_STREAMS = 'urn:ietf:params:xml:ns:xmpp-streams'
var NAME_STREAM = 'stream:stream';


WSConnection.prototype.onopen = function() {
    this.emit('connected')
}

WSConnection.prototype.onmessage = function(msg) {
    if (msg && msg.data){
		var _this = this;
		var dataElement;
		try{
			logger.log('Message <-- '+msg.data);
			dataElement = Element.prototype.parse(JSON.parse(msg.data));
			if(dataElement.name === NAME_STREAM && dataElement.attrs.streamClosed){
			    logger.log("Ending stream",dataElement);
    			if (_this.websocket){
					// @TODO: check onClose method should be called
            		_this.websocket.close();
            	}
			}else if(dataElement.name === NAME_STREAM){
				/* We need those xmlns often, store them extra */
				_this.streamNsAttrs = {};
				for (var k in dataElement.attrs) {
					if (k === 'xmlns' || (k.substr(0, 6) === 'xmlns:')){
						_this.streamNsAttrs[k] = dataElement.attrs[k];
					}
				}
				/*
					Notify in case we don't wait for <stream:features/>
					(Component or non-1.0 streams)
				*/
				_this.emit('streamStart', dataElement.attrs);

			} else if(dataElement.name){
				_this.onStanza(dataElement)
			}
		} catch(err) {
			logger.error('on Msg JSON parse error: ',err);
			err.trace && logger.error(err.trace);
		}
	}
}

WSConnection.prototype.onStanza = function(stanza) {
    if (stanza.is('error', NS_STREAM)) {
        /* TODO: extract error text */
        this.emit('error', stanza)
    } else {
        this.emit('stanza', stanza)
    }
}

WSConnection.prototype.startStream = function() {
    var attrs = {}
    for(var k in this.xmlns) {
        if (this.xmlns.hasOwnProperty(k)) {
            if (!k) {
                attrs.xmlns = this.xmlns[k]
            } else {
                attrs['xmlns:' + k] = this.xmlns[k]
            }
        }
    }
    if (this.xmppVersion)
        attrs.version = this.xmppVersion
    if (this.streamTo)
        attrs.to = this.streamTo
    if (this.streamId)
        attrs.id = this.streamId
    if (this.jid)
        attrs.to = this.jid.domain
    attrs.xmlns = 'jabber:client'
    attrs['xmlns:stream'] = NS_STREAM

    var el = new Element(NAME_STREAM, attrs);
	//el.attr('streamOpened',true);
	this.send(el);

	this.streamOpened = true;

    // make it non-empty to cut the closing tag
    /*
		el.t(' ');
    	var s = el.toString()
    	this.send(s.substr(0, s.indexOf(' </stream:stream>')))
	*/
}

WSConnection.prototype.send = function(stanza) {
    if (stanza.root) stanza = stanza.root()
	var stanzaStr = JSON.stringify(stanza);
    logger.log('Send -->'+stanzaStr)
    this.websocket.send(stanzaStr)
}

WSConnection.prototype.onclose = function() {
    logger.log('OnClose  emit:disconnect:close-->')
    this.emit('disconnect')
    this.emit('close')
}

WSConnection.prototype.end = function() {
    //@TO-DO check Ws connection end
    this.streamOpened = false;
    var el = new Element(NAME_STREAM);
    el.attr('streamClosed',true);
    this.send(el);
    this.emit('end')
    if (this.websocket){
        this.websocket.close()
    }
}

WSConnection.prototype.onerror = function(e) {
    this.emit('error', e)
}

module.exports = WSConnection

},{"./element.js":8,"./util":12,"events":15,"faye-websocket":14}],14:[function(require,module,exports){

},{}],15:[function(require,module,exports){
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

function EventEmitter() {
  this._events = this._events || {};
  this._maxListeners = this._maxListeners || undefined;
}
module.exports = EventEmitter;

// Backwards-compat with node 0.10.x
EventEmitter.EventEmitter = EventEmitter;

EventEmitter.prototype._events = undefined;
EventEmitter.prototype._maxListeners = undefined;

// By default EventEmitters will print a warning if more than 10 listeners are
// added to it. This is a useful default which helps finding memory leaks.
EventEmitter.defaultMaxListeners = 10;

// Obviously not all Emitters should be limited to 10. This function allows
// that to be increased. Set to zero for unlimited.
EventEmitter.prototype.setMaxListeners = function(n) {
  if (!isNumber(n) || n < 0 || isNaN(n))
    throw TypeError('n must be a positive number');
  this._maxListeners = n;
  return this;
};

EventEmitter.prototype.emit = function(type) {
  var er, handler, len, args, i, listeners;

  if (!this._events)
    this._events = {};

  // If there is no 'error' event listener then throw.
  if (type === 'error') {
    if (!this._events.error ||
        (isObject(this._events.error) && !this._events.error.length)) {
      er = arguments[1];
      if (er instanceof Error) {
        throw er; // Unhandled 'error' event
      }
      throw TypeError('Uncaught, unspecified "error" event.');
    }
  }

  handler = this._events[type];

  if (isUndefined(handler))
    return false;

  if (isFunction(handler)) {
    switch (arguments.length) {
      // fast cases
      case 1:
        handler.call(this);
        break;
      case 2:
        handler.call(this, arguments[1]);
        break;
      case 3:
        handler.call(this, arguments[1], arguments[2]);
        break;
      // slower
      default:
        len = arguments.length;
        args = new Array(len - 1);
        for (i = 1; i < len; i++)
          args[i - 1] = arguments[i];
        handler.apply(this, args);
    }
  } else if (isObject(handler)) {
    len = arguments.length;
    args = new Array(len - 1);
    for (i = 1; i < len; i++)
      args[i - 1] = arguments[i];

    listeners = handler.slice();
    len = listeners.length;
    for (i = 0; i < len; i++)
      listeners[i].apply(this, args);
  }

  return true;
};

EventEmitter.prototype.addListener = function(type, listener) {
  var m;

  if (!isFunction(listener))
    throw TypeError('listener must be a function');

  if (!this._events)
    this._events = {};

  // To avoid recursion in the case that type === "newListener"! Before
  // adding it to the listeners, first emit "newListener".
  if (this._events.newListener)
    this.emit('newListener', type,
              isFunction(listener.listener) ?
              listener.listener : listener);

  if (!this._events[type])
    // Optimize the case of one listener. Don't need the extra array object.
    this._events[type] = listener;
  else if (isObject(this._events[type]))
    // If we've already got an array, just append.
    this._events[type].push(listener);
  else
    // Adding the second element, need to change to array.
    this._events[type] = [this._events[type], listener];

  // Check for listener leak
  if (isObject(this._events[type]) && !this._events[type].warned) {
    var m;
    if (!isUndefined(this._maxListeners)) {
      m = this._maxListeners;
    } else {
      m = EventEmitter.defaultMaxListeners;
    }

    if (m && m > 0 && this._events[type].length > m) {
      this._events[type].warned = true;
      console.error('(node) warning: possible EventEmitter memory ' +
                    'leak detected. %d listeners added. ' +
                    'Use emitter.setMaxListeners() to increase limit.',
                    this._events[type].length);
      if (typeof console.trace === 'function') {
        // not supported in IE 10
        console.trace();
      }
    }
  }

  return this;
};

EventEmitter.prototype.on = EventEmitter.prototype.addListener;

EventEmitter.prototype.once = function(type, listener) {
  if (!isFunction(listener))
    throw TypeError('listener must be a function');

  var fired = false;

  function g() {
    this.removeListener(type, g);

    if (!fired) {
      fired = true;
      listener.apply(this, arguments);
    }
  }

  g.listener = listener;
  this.on(type, g);

  return this;
};

// emits a 'removeListener' event iff the listener was removed
EventEmitter.prototype.removeListener = function(type, listener) {
  var list, position, length, i;

  if (!isFunction(listener))
    throw TypeError('listener must be a function');

  if (!this._events || !this._events[type])
    return this;

  list = this._events[type];
  length = list.length;
  position = -1;

  if (list === listener ||
      (isFunction(list.listener) && list.listener === listener)) {
    delete this._events[type];
    if (this._events.removeListener)
      this.emit('removeListener', type, listener);

  } else if (isObject(list)) {
    for (i = length; i-- > 0;) {
      if (list[i] === listener ||
          (list[i].listener && list[i].listener === listener)) {
        position = i;
        break;
      }
    }

    if (position < 0)
      return this;

    if (list.length === 1) {
      list.length = 0;
      delete this._events[type];
    } else {
      list.splice(position, 1);
    }

    if (this._events.removeListener)
      this.emit('removeListener', type, listener);
  }

  return this;
};

EventEmitter.prototype.removeAllListeners = function(type) {
  var key, listeners;

  if (!this._events)
    return this;

  // not listening for removeListener, no need to emit
  if (!this._events.removeListener) {
    if (arguments.length === 0)
      this._events = {};
    else if (this._events[type])
      delete this._events[type];
    return this;
  }

  // emit removeListener for all listeners on all events
  if (arguments.length === 0) {
    for (key in this._events) {
      if (key === 'removeListener') continue;
      this.removeAllListeners(key);
    }
    this.removeAllListeners('removeListener');
    this._events = {};
    return this;
  }

  listeners = this._events[type];

  if (isFunction(listeners)) {
    this.removeListener(type, listeners);
  } else {
    // LIFO order
    while (listeners.length)
      this.removeListener(type, listeners[listeners.length - 1]);
  }
  delete this._events[type];

  return this;
};

EventEmitter.prototype.listeners = function(type) {
  var ret;
  if (!this._events || !this._events[type])
    ret = [];
  else if (isFunction(this._events[type]))
    ret = [this._events[type]];
  else
    ret = this._events[type].slice();
  return ret;
};

EventEmitter.listenerCount = function(emitter, type) {
  var ret;
  if (!emitter._events || !emitter._events[type])
    ret = 0;
  else if (isFunction(emitter._events[type]))
    ret = 1;
  else
    ret = emitter._events[type].length;
  return ret;
};

function isFunction(arg) {
  return typeof arg === 'function';
}

function isNumber(arg) {
  return typeof arg === 'number';
}

function isObject(arg) {
  return typeof arg === 'object' && arg !== null;
}

function isUndefined(arg) {
  return arg === void 0;
}

},{}]},{},[1]);
