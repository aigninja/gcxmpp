#gc-xmpp

Xmpp client for Gameclosure

With using [Browserify](https://github.com/substack/node-browserify). To create a bundle run `grunt browserify`.

== Command to generate file == 
 browserify index.js > xmpp.js

### Testing

Install the dev dependencies, then...

```npm test```


## Features

* Client authentication with SASL DIGEST-MD5, PLAIN, ANONYMOUS, X-FACEBOOK-PLATFORM
* `_xmpp-client._tcp` SRV record support
* Even runs in the Browser.

### Building XML Elements

Strophe.js' XML Builder is very convenient for producing XMPP
stanzas. ltx includes it in a much more primitive way: the
`c()`, `cnode()` and `t()` methods can be called on any *Element*
object, returning the new child element.

This can be confusing: in the end, you will hold the last-added child
until you use `up()`, a getter for the parent. `Connection.send()`
first invokes `tree()` to retrieve the uppermost parent, the XMPP
stanza, before sending it out the wire.

# Keepalives

Rather than send empty packets in order to keep any socket alive please try the following:

```
this.client.connection.socket.setTimeout(0)
this.client.connection.socket.setKeepAlive(true, 10000)
```
Where `this.client` is the result of `new require('node-xmpp-client')()`.
