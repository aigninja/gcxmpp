'use strict';

var util = require('./util');

/**
 * Attributes are in the element.attrs object. Children is a list of
 * either other Elements or Strings for text content.
 **/

function Element(name, attrs) {
    this.name = name
    this.parent = null
    this.attrs = attrs || {}
    this.children = []
}

/*** Accessors ***/

/**
 * if (element.is('message', 'jabber:client')) ...
 **/

Element.prototype.is = function(name, xmlns) {
    return (this.getName() === name) &&
        (!xmlns || (this.getNS() === xmlns))
}

/* without prefix */
Element.prototype.getName = function() {
    if (this.name.indexOf(':') >= 0)
        return this.name.substr(this.name.indexOf(':') + 1)
    else
        return this.name
}

/**
 * retrieves the namespace of the current element, upwards recursively
 **/
Element.prototype.getNS = function() {
    if (this.name.indexOf(':') >= 0) {
        var prefix = this.name.substr(0, this.name.indexOf(':'))
        return this.findNS(prefix)
    } else {
        return this.findNS()
    }
}

/**
 * find the namespace to the given prefix, upwards recursively
 **/
Element.prototype.findNS = function(prefix) {
    if (!prefix) {
        /* default namespace */
        if (this.attrs.xmlns)
            return this.attrs.xmlns
        else if (this.parent)
            return this.parent.findNS()
    } else {
        /* prefixed namespace */
        var attr = 'xmlns:' + prefix
        if (this.attrs[attr])
            return this.attrs[attr]
        else if (this.parent)
            return this.parent.findNS(prefix)
    }
}

/**
 * Recursiverly gets all xmlns defined, in the form of {url:prefix}
 **/
Element.prototype.getXmlns = function() {
    var namespaces = {}

    if (this.parent)
        namespaces = this.parent.getXmlns()

    for (var attr in this.attrs) {
        var m = attr.match('xmlns:?(.*)')
        if (this.attrs.hasOwnProperty(attr) && m) {
            namespaces[this.attrs[attr]] = m[1]
        }
    }
    return namespaces
}


/**
 * xmlns can be null, returns the matching attribute.
 **/
Element.prototype.getAttr = function(name, xmlns) {
    if (!xmlns)
        return this.attrs[name]

    var namespaces = this.getXmlns()

    if (!namespaces[xmlns])
        return null

    return this.attrs[[namespaces[xmlns], name].join(':')]
}

/**
 * xmlns can be null
 **/
Element.prototype.getChild = function(name, xmlns) {
    return this.getChildren(name, xmlns)[0]
}

/**
 * xmlns can be null
 **/
Element.prototype.getChildren = function(name, xmlns) {
    var result = []
    for (var i = 0; i < this.children.length; i++) {
        var child = this.children[i]
        if (child.getName &&
            (child.getName() === name) &&
            (!xmlns || (child.getNS() === xmlns)))
            result.push(child)
    }
    return result
}

/**
 * xmlns and recursive can be null
 **/
Element.prototype.getChildByAttr = function(attr, val, xmlns, recursive) {
    return this.getChildrenByAttr(attr, val, xmlns, recursive)[0]
}

/**
 * xmlns and recursive can be null
 **/
Element.prototype.getChildrenByAttr = function(attr, val, xmlns, recursive) {
    var result = []
    for (var i = 0; i < this.children.length; i++) {
        var child = this.children[i]
        if (child.attrs &&
            (child.attrs[attr] === val) &&
            (!xmlns || (child.getNS() === xmlns)))
            result.push(child)
        if (recursive && child.getChildrenByAttr) {
            result.push(child.getChildrenByAttr(attr, val, xmlns, true))
        }
    }
    if (recursive) result = [].concat.apply([], result)
    return result
}

Element.prototype.getChildrenByFilter = function(filter, recursive) {
    var result = []
    for (var i = 0; i < this.children.length; i++) {
        var child = this.children[i]
        if (filter(child))
            result.push(child)
        if (recursive && child.getChildrenByFilter){
            result.push(child.getChildrenByFilter(filter, true))
        }
    }
    if (recursive) {
        result = [].concat.apply([], result)
    }
    return result
}

Element.prototype.getText = function() {
    var text = ''
    for (var i = 0; i < this.children.length; i++) {
        var child = this.children[i]
        if ((child.constructor == String) || (typeof child === 'number')) {
            text += child
        }
    }
    return text
}

Element.prototype.getChildText = function(name, xmlns) {
    var child = this.getChild(name, xmlns)
    return child ? child.getText() : null
}

/**
 * Return all direct descendents that are Elements.
 * This differs from `getChildren` in that it will exclude text nodes,
 * processing instructions, etc.
 */
Element.prototype.getChildElements = function() {
    return this.getChildrenByFilter(function(child) {
        return child instanceof Element
    })
}

/*** Builder ***/

/** returns uppermost parent */
Element.prototype.root = function() {
    if (this.parent)
        return this.parent.root()
    else
        return this
}

Element.prototype.tree = Element.prototype.root

/** just parent or itself */
Element.prototype.up = function() {
    if (this.parent)
        return this.parent
    else
        return this
}

Element.prototype._getElement = function(name, attrs) {
    var element = new Element(name, attrs)
    return element
}

/** create child node and return it */
Element.prototype.c = function(name, attrs) {
    return this.cnode(this._getElement(name, attrs))
}

Element.prototype.cnode = function(child) {
    this.children.push(child)
    child.parent = this
    return child
}

/** add text node and return element */
Element.prototype.t = function(text) {
    this.children.push(text)
    return this
}

/*** Manipulation ***/

/**
 * Either:
 *   el.remove(childEl)
 *   el.remove('author', 'urn:...')
 */
Element.prototype.remove = function(el, xmlns) {
    var filter
    if (typeof el === 'string') {
        /* 1st parameter is tag name */
        filter = function(child) {
            return !(child.is &&
                 child.is(el, xmlns))
        }
    } else {
        /* 1st parameter is element */
        filter = function(child) {
            return child !== el
        }
    }
    this.children = this.children.filter(filter)
    return this
}

/**
 * To use in case you want the same XML data for separate uses.
 * Please refrain from this practise unless you know what you are
 * doing. Building XML with ltx is easy!
 */
Element.prototype.clone = function() {
    var clone = this._getElement(this.name, {})
    for (var k in this.attrs) {
        if (this.attrs.hasOwnProperty(k))
            clone.attrs[k] = this.attrs[k]
    }
    for (var i = 0; i < this.children.length; i++) {
        var child = this.children[i]
        clone.cnode(child.clone ? child.clone() : child)
    }
    return clone
}

Element.prototype.text = function(val) {
    if (val && this.children.length === 1) {
        this.children[0] = val
        return this
    }
    return this.getText()
}

Element.prototype.attr = Element.prototype.setAttr = function(attr, val) {
    if (((typeof val !== 'undefined') || (val === null))) {
        if (!this.attrs) {
            this.attrs = {}
        }
        this.attrs[attr] = val
        return this
    }
    return this.attrs[attr]
}

/*** Serialization ***/

Element.prototype.toString = function() {
    return JSON.stringify(this.toJSON());
}

Element.prototype.toJSON = function() {
    return {
        name: this.name,
        attrs: this.attrs,
        children: this.children.map(function(child) {
            return child && child.toJSON ? child.toJSON() : child;
        })
    }
}

Element.prototype.write = function(writer) {
    writer(this.toJSON());
}

/*
 TO-DO: Write testcase for it
*/

var parseXMLElement = function(jsonXMl) {
    var element;
	if(jsonXMl.constructor == String){
		 element = new String(jsonXMl);
	} else if(jsonXMl.name){
		element = new Element(jsonXMl.name, jsonXMl.attrs);
		if(jsonXMl.children && jsonXMl.children.length > 0){
		    var length = jsonXMl.children.length;
			for (var i = 0; i < length; i++) {
			    var child = jsonXMl.children[i];
				if(child){
				    var elm  = parseXMLElement(child);
	                elm && element.cnode(elm);
				}
			}
		}
	}
    return element;
};

Element.prototype.parse = parseXMLElement

function Stanza(name, attrs) {
    Element.call(this, name, attrs)
}

Stanza.TYPE = {
    MESSAGE: 'message',
    IQ: 'iq',
    PRESENCE: 'presence',
    GROUP: 'group'
}

util.inherits(Stanza, Element)

/**
 * Common attribute getters/setters for all stanzas
 */

Object.defineProperty(Stanza.prototype, 'from', {
    get: function() {
        return this.attrs.from
    },

    set: function(from) {
        this.attrs.from = from
    }
});

Object.defineProperty(Stanza.prototype, 'to', {
    get: function() {
        return this.attrs.to
    },

    set: function(to) {
        this.attrs.to = to
    }
});

Object.defineProperty(Stanza.prototype, 'id', {
    get: function() {
        return this.attrs.id
    },

    set: function(id) {
        this.attrs.id = id
    }
});

Object.defineProperty(Stanza.prototype, 'type', {
    get: function() {
        return this.attrs.type
    },

    set: function(type) {
        this.attrs.type = type
    }
});

var stanzaNumber = 0;
Stanza.prototype.generateID = function (prefix){
    prefix = prefix || '';
    return prefix+'_'+(Math.random()*0xffffffff)+'_'+Date.now()+'_'+(stanzaNumber++);
};

/**
 * Stanza kinds
 */

function Message(attrs) {
    Stanza.call(this, Stanza.TYPE.MESSAGE, attrs)
}

util.inherits(Message, Stanza);

Message.TYPE = {
    SERVER_RECEIVE_ACK : 'srvr_rcv_ack',
    USER_RECEIVE_ACK : 'usr_rcv_ack',
    MESSAGE: 'msg'
}

Message.create = function(opts){
    opts.type = opts.type || Message.TYPE.MESSAGE;
    opts.created = opts.created || Date.now();
    opts.id = opts.id || Stanza.prototype.generateID('clnt_msgcrt');
    var message = new Message({
        from: opts.from,
        to: opts.to,
        id: opts.id,
        'type': opts.type,
        'created': opts.created
    }).c('body',{}).t(opts.text);
    opts.ackid && message.root().attr('ackid',opts.ackid);
    return message;
};


function Presence(attrs) {
    Stanza.call(this, Stanza.TYPE.PRESENCE, attrs)
}

util.inherits(Presence, Stanza)

Presence.create = function(opts){
    opts.created = opts.created || Date.now();
    opts.id = opts.id || Stanza.prototype.generateID('clnt_prsnccrt');
    var presence = new Presence({
        id: opts.id,
        'created': opts.created
    });
    opts.from && presence.attr('from',opts.from);
    opts.to && presence.attr('to',opts.to);
    opts.type && presence.attr('type',opts.type);
    opts.status && presence.attr('status',opts.status);
    opts.lastOnline && presence.attr('lastOnline',opts.lastOnline);
    return presence;
};

/*
     DEFAULT type is Available(When not specified)
     <xs:attribute name='type' use='optional'>
        <xs:simpleType>
          <xs:restriction base='xs:NCName'>
            <xs:enumeration value='error'/>
            <xs:enumeration value='probe'/>
            <xs:enumeration value='subscribe'/>
            <xs:enumeration value='subscribed'/>
            <xs:enumeration value='unavailable'/>
            <xs:enumeration value='unsubscribe'/>
            <xs:enumeration value='unsubscribed'/>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
*/

Presence.TYPE = {
    UNAVAILABLE: 'unavailable',
    CONTACTSPRESENCE: 'contactspresence',
    SUBSCRIBE: 'subscribe',
    SUBSCRIBED: 'subscribed',
    UNSUBSCRIBE: 'unsubscribe',
    UNSUBSCRIBED: 'unsubscribed',
    PROBE: 'probe',
    ERROR: 'error',
};

Presence.STATUS = {
    ONLINE: 'online',
    OFFLINE: 'offline',
    AWAY: 'away'
};

function Iq(attrs) {
    Stanza.call(this, Stanza.TYPE.IQ, attrs)
}

Iq.TYPE = {
    RESULT: 'result',
    GET: 'get',
    SET: 'set',
    ERROR: 'error'
};


Iq.create = function(opts){
    var stanzaId = opts.id || Iq.prototype.generateID('srvr_iqcrt');
    opts.created = opts.created || Date.now();
    opts.queryType = opts.queryType;

    var iqStanza = new Iq({
            type: opts.type || 'result',
            id: stanzaId , 'to': opts.to ,
            'created': opts.created
        }).c('query',{ 'xmlns':opts.queryType});

    return iqStanza; // It stanza new create but pointer at query node
};

Iq.NAMESPACE = {
    PUSH_NOTIFY: 'iq:notify:push',
    APPLICATION: 'iq:application',
    PING:'urn:xmpp:ping'
};

util.inherits(Iq, Stanza);

function Group(attrs) {
    Stanza.call(this, Stanza.TYPE.GROUP, attrs)
}

Group.TYPE = {
    STANZA_GROUP: 'msg_grp',
    STANZA_GROUP_ACK: 'msg_grp_usr_ack'
}

util.inherits(Group, Stanza);

Group.create = function(opts){
    opts.type = opts.type || Group.TYPE.STANZA_GROUP;
    opts.created = opts.created || Date.now();
    opts.id = opts.id || Stanza.prototype.generateID('clnt_grpcrt');
    var grp = new Group({
        to: opts.to,
        id: opts.id,
        'created': opts.created,
        'type': opts.type
    });
    opts.ackid && grp.attr('ackid',opts.ackid);
    return grp;
}

Stanza.NAMESPACE = {

    NS_CLIENT: 'jabber:client',
    NS_XMPP_SASL: 'urn:ietf:params:xml:ns:xmpp-sasl',
    NS_REGISTER: 'jabber:iq:register',
    NS_SESSION: 'urn:ietf:params:xml:ns:xmpp-session',
    NS_BIND: 'urn:ietf:params:xml:ns:xmpp-bind',
    NS_STANZAS: 'urn:ietf:params:xml:ns:xmpp-stanzas',
    NS_XMPP_TLS: 'urn:ietf:params:xml:ns:xmpp-tls',
    NS_STREAM: 'http://etherx.jabber.org/streams',
    NS_XMPP_STREAMS: 'urn:ietf:params:xml:ns:xmpp-streams',
    NAME_STREAM: 'stream:stream'

}

/*
     More Info: http://xmpp.org/extensions/xep-0086.html
     Erro code:

      <error type='modify'>
        <bad-request xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>
      </error>

            <bad-request/>	modify	400
            <conflict/>	cancel	409
    <feature-not-implemented/>	cancel	501
    <forbidden/>	auth	403
    <gone/>	modify	302 (permanent)
            <internal-server-error/>	wait	500
            <item-not-found/>	cancel	404
    <jid-malformed/>	modify	400
            <not-acceptable/>	modify	406
    <not-allowed/>	cancel	405
    <not-authorized/>	auth	401
    <payment-required/>	auth	402
    <recipient-unavailable/>	wait	404
    <redirect/>	modify	302 (temporary)
    <registration-required/>	auth	407
    <remote-server-not-found/>	cancel	404
    <remote-server-timeout/>	wait	504
    <resource-constraint/>	wait	500
    <service-unavailable/>	cancel	503
    <subscription-required/>	auth	407
    <undefined-condition/>	[any]	500
    <unexpected-request/>	wait	400

*/

Stanza.ERROR = {
    BAD_REQUEST: {
        'type': 'modify',
        'code': '400',
        'condition': 'bad-request'
    },

    CONFLICT: {
        'type': 'cancel',
        'code': '409',
        'condition': 'conflict'
    },

    NOT_ACCEPTABLE: {
        'type': 'modify',
        'code': '406',
        'condition': 'not-acceptable'
    },

    NOT_FOUND: {
        'type': 'cancel',
        'code': '404',
        'condition': 'item-not-found'
    },

    INTERNAL_SERVER_ERROR: {
        'type': 'wait',
        'code': '500',
        'condition': 'internal-server-error'
    }
};

Stanza.prototype.addError = function(err){
    if(this.root){
        this.root();
    };
    this.attrs.type = 'error';
    this.c('error', {
            code: '' + err.code,
            type: err.type,
            message: err.message || err.text
        })
        .c(err.condition || '' , {
            xmlns: Stanza.NAMESPACE.NS_STANZAS
        });
};


exports.Element = Element
exports.Stanza = Stanza
exports.Message = Message
exports.Presence = Presence
exports.Iq = Iq
exports.Group = Group
