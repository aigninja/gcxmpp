'use strict';

var   util = require('./util')
  	, Element	= require('./element.js').Element
  	
var logger =  util.logger;  	

function WSConnection(opts) {
    EventEmitter.call(this)

    this.url = opts.websocket.url
    this.jid = opts.jid
    this.xmlns = {}

    this.websocket = new WebSocket(this.url)
	/*
	Earlier code was like this
	this.websocket = new WebSocket(this.url, ['xmpp'])
	*/
    this.websocket.onopen = this.onopen.bind(this)
    this.websocket.onmessage = this.onmessage.bind(this)
    this.websocket.onclose = this.onclose.bind(this)
    this.websocket.onerror = this.onerror.bind(this)
}

util.inherits(WSConnection, EventEmitter)

WSConnection.prototype.maxStanzaSize = 65535
WSConnection.prototype.xmppVersion = '1.0'

var NS_XMPP_TLS = 'urn:ietf:params:xml:ns:xmpp-tls'
var NS_STREAM = 'http://etherx.jabber.org/streams'
var NS_XMPP_STREAMS = 'urn:ietf:params:xml:ns:xmpp-streams'
var NAME_STREAM = 'stream:stream';


WSConnection.prototype.onopen = function() {
    this.emit('connected')
}

WSConnection.prototype.onmessage = function(msg) {
    if (msg && msg.data){
		var _this = this;
		var dataElement;
		try{
			logger.log('Message <-- '+msg.data);
			dataElement = Element.prototype.parse(JSON.parse(msg.data));
			if(dataElement.name === NAME_STREAM && dataElement.attrs.streamClosed){
			    logger.log("Ending stream",dataElement);
    			if (_this.websocket){
					// @TODO: check onClose method should be called
            		_this.websocket.close();
            	}
			}else if(dataElement.name === NAME_STREAM){
				/* We need those xmlns often, store them extra */
				_this.streamNsAttrs = {};
				for (var k in dataElement.attrs) {
					if (k === 'xmlns' || (k.substr(0, 6) === 'xmlns:')){
						_this.streamNsAttrs[k] = dataElement.attrs[k];
					}
				}
				/*
					Notify in case we don't wait for <stream:features/>
					(Component or non-1.0 streams)
				*/
				_this.emit('streamStart', dataElement.attrs);
				
			} else if(dataElement.name){
				_this.onStanza(dataElement)
			}
		} catch(err) {
			logger.error('on Msg JSON parse error: ',err);
			err.trace && logger.error(err.trace);
		}
	}
}

WSConnection.prototype.onStanza = function(stanza) {
    if (stanza.is('error', NS_STREAM)) {
        /* TODO: extract error text */
        this.emit('error', stanza)
    } else {
        this.emit('stanza', stanza)
    }
}

WSConnection.prototype.startStream = function() {
    var attrs = {}
    for(var k in this.xmlns) {
        if (this.xmlns.hasOwnProperty(k)) {
            if (!k) {
                attrs.xmlns = this.xmlns[k]
            } else {
                attrs['xmlns:' + k] = this.xmlns[k]
            }
        }
    }
    if (this.xmppVersion)
        attrs.version = this.xmppVersion
    if (this.streamTo)
        attrs.to = this.streamTo
    if (this.streamId)
        attrs.id = this.streamId
    if (this.jid)
        attrs.to = this.jid.domain
    attrs.xmlns = 'jabber:client'
    attrs['xmlns:stream'] = NS_STREAM

    var el = new Element(NAME_STREAM, attrs);
	//el.attr('streamOpened',true);
	this.send(el);

	this.streamOpened = true;

    // make it non-empty to cut the closing tag
    /*
		el.t(' ');
    	var s = el.toString()
    	this.send(s.substr(0, s.indexOf(' </stream:stream>')))
	*/
}

WSConnection.prototype.send = function(stanza) {
    if (stanza.root) stanza = stanza.root()
	var stanzaStr = JSON.stringify(stanza);
    logger.log('Send -->'+stanzaStr)
    this.websocket.send(stanzaStr)
}

WSConnection.prototype.onclose = function() {
    logger.log('OnClose  emit:disconnect:close-->')
    this.emit('disconnect')
    this.emit('close')
}

WSConnection.prototype.end = function() {
    //@TO-DO check Ws connection end
    this.streamOpened = false;
    var el = new Element(NAME_STREAM);
    el.attr('streamClosed',true);
    this.send(el);
    this.emit('end')
    if (this.websocket){
        this.websocket.close()
    }
}

WSConnection.prototype.onerror = function(e) {
    this.emit('error', e)
}

module.exports = WSConnection
