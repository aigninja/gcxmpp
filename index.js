'use strict';

var Session = require('./lib/session')
  , JID = require('./lib/jid')
  , sasl = require('./lib/sasl')
  , Anonymous = require('./lib/authentication/anonymous')
  , Plain = require('./lib/authentication/plain')
  , XOAuth2 = require('./lib/authentication/xoauth2')
  , External = require('./lib/authentication/external')
  , util = require('./lib/util')
  , Base64 = require('./lib/base64').Base64
  , element  = require('./lib/element')
  
var Group = element.Group
  , Stanza =  element.Stanza
  , Iq =  element.Iq
  , Message = element.Message
  , Element = element.Element
  , Presence = element.Presence
  
var logger =  util.logger;

var STATE_PREAUTH = 0
  , STATE_AUTH = 1
  , STATE_AUTHED = 2
  , STATE_BIND = 3
  , STATE_SESSION = 4
  , STATE_ONLINE = 5

var IQID_SESSION = 'sess'
  , IQID_BIND = 'bind'

function Client(options) {
    this.options = {}
    if (options) this.options = options
    this.availableSaslMechanisms = [
        XOAuth2, External, Plain, Anonymous
    ]

    if (this.options.autostart !== false)
        this.connect()
}

util.inherits(Client, Session)

Client.NS_CLIENT = Stanza.NAMESPACE.NS_CLIENT


/* @TODO:
    We should prefer native atob and btoa over  Base64.decode and Base64.encode
	search if latest GC engine supports these functions
*/

/* jshint latedef: false */
/* jshint -W079 */
/* jshint -W020 */

var decode64, encode64;

decode64 = Base64.decode;
encode64 = Base64.encode;


/*if (typeof btoa === 'undefined') {
    var btoa = null
    var atob = null
}

if (typeof btoa === 'function') {
    decode64 = function(encoded) {
        return atob(encoded)
    }
} else {
    logger.error('No atob :',atob);
}
if (typeof atob === 'function') {
    encode64 = function(decoded) {
        return btoa(decoded)
    }
} else {
    logger.warn('No btoa :',btoa);
}*/

Client.prototype.connect = function() {

        this.options.xmlns = Stanza.NAMESPACE.NS_CLIENT
        /* jshint camelcase: false */
        delete this.did_bind
        delete this.did_session

        this.state = STATE_PREAUTH
        this.on('end', function() {
            this.state = STATE_PREAUTH
            delete this.did_bind
            delete this.did_session
        })

        this.on('error', function(err) {
            logger.error("Error occured while Connecting JMPP Client",err);
        });
        
        Session.call(this, this.options)
        this.options.jid = this.jid

        this.connection.on('disconnect', function(error) {
            this.state = STATE_PREAUTH
            if (!this.connection.reconnect) {
                if (error) this.emit('error', error)
                this.emit('offline')
            }
            delete this.did_bind
            delete this.did_session
        }.bind(this))

        // If server and client have multiple possible auth mechanisms
        // we try to select the preferred one
        if (this.options.preferred) {
            this.preferredSaslMechanism = this.options.preferred
        } else {
            this.preferredSaslMechanism = 'External'
        }

        var mechs = sasl.detectMechanisms(this.options, this.availableSaslMechanisms)
        this.availableSaslMechanisms = mechs
}

Client.prototype.onStanza = function(stanza) {
    var stanzaId = stanza.attrs.id;
    var to = stanza.attrs.to || '';
    
    if ((this.state !== STATE_ONLINE) && stanza.is('features')) {
        this.streamFeatures = stanza
        this.useFeatures()
    } else if (this.state === STATE_PREAUTH) {
        this.emit('stanza:preauth', stanza)
    } else if (this.state === STATE_AUTH) {
        this._handleAuthState(stanza)
    } else if ((this.state === STATE_BIND) && stanza.is('iq') && (stanzaId === IQID_BIND)) {
        this._handleBindState(stanza)
    } else if ((this.state === STATE_SESSION) && (true === stanza.is('iq')) &&
        (stanzaId === IQID_SESSION)) {
        this._handleSessionState(stanza)
    } else if (stanza.name === 'stream:error') {
        if (!this.reconnect){
            this.emit('error', stanza)
        }
    } else if (this.state === STATE_ONLINE) {
        var type = stanza.attrs.type;;
        
        if(!stanza.is( Stanza.TYPE.GROUP)){
            this.emit('stanza', stanza);
        }
        /*
            Reply acknowldgment for Message & Group stanza receipnt  
        */
        var replyStanzaId;
        
        if(stanza.is(Stanza.TYPE.MESSAGE)){
            
            if(!type){
                type = Message.TYPE.MESSAGE;
            }
            
            var ackMsg;
            if(type == Message.TYPE.MESSAGE) {
                replyStanzaId = Message.prototype.generateID(Message.TYPE.USER_RECEIVE_ACK);
                var fromBareAddress = stanza.attrs.from && new JID(stanza.attrs.from).bare().toString();
                ackMsg = Message.create({
                    to: fromBareAddress,
                    type: Message.TYPE.USER_RECEIVE_ACK,
                    created: Date.now(),
                    ackid: stanzaId,
                    id: replyStanzaId
                });
                this.send(ackMsg);
            }
        } else if(stanza.is( Stanza.TYPE.GROUP) && type == Group.TYPE.STANZA_GROUP){
            var groupStanzas = stanza.getChildElements();
            var totalStanza = groupStanzas.length;
            
            if(totalStanza > 0){
                var childStanza;
				var stanzas = [];
                var groupStanzaReply = Group.create({
                    type: Group.TYPE.STANZA_GROUP_ACK,
                    created: Date.now(),
                    ackid: stanzaId,
                    id: Message.prototype.generateID(Group.TYPE.STANZA_GROUP_ACK)
                });
                
                for(var i = 0; i < totalStanza; i++){
                    if(groupStanzas[i]){
                         try{
							 childStanza = Stanza.prototype.parse(groupStanzas[i]);
							 stanzas.push(childStanza);
							 var childType = childStanza.attrs.type || Message.TYPE.MESSAGE;
							 if(childStanza.is(Stanza.TYPE.MESSAGE) && childType == Message.TYPE.MESSAGE){
								var fromBareAddress = childStanza.attrs.from && new JID(childStanza.attrs.from).bare().toString();
								groupStanzaReply.c(Stanza.TYPE.MESSAGE,{
									to: fromBareAddress,
									type: Message.TYPE.USER_RECEIVE_ACK,
									created: Date.now(),
									ackid: childStanza.attrs.id,
									id:  Message.prototype.generateID(Message.TYPE.USER_RECEIVE_ACK)
								}).up();
							 };
						}catch(e){
                            logger.error("Group stanza parsing error",e);
                         };
                    };
                };

                this.send(groupStanzaReply);

				for(var i = 0; i < stanzas.length; i++){
					this.emit('stanza',stanzas[i]);
				}
            }
        } else if(stanza.is( Stanza.TYPE.GROUP) && type == Group.TYPE.STANZA_GROUP_ACK){
            var groupStanzas = stanza.getChildElements();
            var totalStanza = groupStanzas.length;

            if(totalStanza > 0){
                var childStanza;
                for(var i = 0; i < totalStanza; i++){
                    if(groupStanzas[i]){
                        try{
                            childStanza = Stanza.prototype.parse(groupStanzas[i])
                        }catch(e){
                            logger.error("Group stanza parsing error",e);
                        };
                        this.emit('stanza',childStanza);
                    };
                };
            }
        }
    }
}

Client.prototype._handleSessionState = function(stanza) {
    if (stanza.attrs.type === 'result') {
        this.state = STATE_AUTHED
        /* jshint camelcase: false */
        this.did_session = true

        /* no stream restart, but next feature (most probably
           we'll go online next) */
       // this.useFeatures()

        this.state = STATE_ONLINE
        this.emit('online', stanza)
    } else {
        this.emit('error', 'CANT_BIND_RESOURSE_HANDLE_SESSION')
    }
}

Client.prototype._handleBindState = function(stanza) {
    if (stanza.attrs.type === 'result') {
        this.state = STATE_AUTHED
        /*jshint camelcase: false */
        this.did_bind = true

        var bindEl = stanza.getChild('bind', Stanza.NAMESPACE.NS_BIND)
        if (bindEl && bindEl.getChild('jid')) {
            this.jid = new JID(bindEl.getChild('jid').getText())
        }

        /* no stream restart, but next feature */
        this.useFeatures()
    } else {
        this.emit('error', 'CANT_BIND_RESOURSE_BIND_SESSION')
    }
}

Client.prototype._handleAuthState = function(stanza) {
    if (stanza.is('challenge', Stanza.NAMESPACE.NS_XMPP_SASL)) {
        var challengeMsg = decode64(stanza.getText())
        var responseMsg = encode64(this.mech.challenge(challengeMsg))
        var response = new Stanza(
            'response', { xmlns: Stanza.NAMESPACE.NS_XMPP_SASL }
        ).t(responseMsg)
        this.send(response)
    } else if (stanza.is('success', Stanza.NAMESPACE.NS_XMPP_SASL)) {
        this.mech = null
        this.state = STATE_AUTHED
        this.emit('auth')
    } else {
        this.emit('error', 'XMPP authentication failure')
    }
}

Client.prototype._handlePreAuthState = function() {
    this.state = STATE_AUTH
    var offeredMechs = this.streamFeatures.
        getChild('mechanisms', Stanza.NAMESPACE.NS_XMPP_SASL).
        getChildren('mechanism', Stanza.NAMESPACE.NS_XMPP_SASL).
        map(function(el) { return el.getText() })
    this.mech = sasl.selectMechanism(
        offeredMechs,
        this.preferredSaslMechanism,
        this.availableSaslMechanisms
    )
    if (this.mech) {
        this.mech.authzid = this.jid.bare().toString()
        this.mech.authcid = this.jid.user
        this.mech.password = this.password
        /*jshint camelcase: false */
        this.mech.api_key = this.api_key
        this.mech.access_token = this.access_token
        this.mech.oauth2_token = this.oauth2_token
        this.mech.oauth2_auth = this.oauth2_auth
        this.mech.realm = this.jid.domain  // anything?
        if (this.actAs) this.mech.actAs = this.actAs.user
        this.mech.digest_uri = 'xmpp/' + this.jid.domain
        var authMsg = encode64(this.mech.auth())
        var attrs = this.mech.authAttrs()
        attrs.xmlns = Stanza.NAMESPACE.NS_XMPP_SASL
        attrs.mechanism = this.mech.name
        this.send(new Stanza('auth', attrs)
            .t(authMsg))
    } else {
        this.emit('error', 'NO_SASL')
    }
}

/**
 * Either we just received <stream:features/>, or we just enabled a
 * feature and are looking for the next.
 */
Client.prototype.useFeatures = function() {
    /* jshint camelcase: false */
    if ((this.state === STATE_PREAUTH) && this.register) {
        delete this.register
        this.doRegister()
    } else if ((this.state === STATE_PREAUTH) &&
        this.streamFeatures.getChild('mechanisms', Stanza.NAMESPACE.NS_XMPP_SASL)) {
        this._handlePreAuthState()
    } else if ((this.state === STATE_AUTHED) &&
               !this.did_bind &&
               this.streamFeatures.getChild('bind', Stanza.NAMESPACE.NS_BIND)) {
        this.state = STATE_BIND
        var bindEl = new Stanza(
            'iq',
            { type: 'set', id: IQID_BIND }
        ).c('bind', { xmlns: Stanza.NAMESPACE.NS_BIND })
        if (this.jid.resource)
            bindEl.c('resource').t(this.jid.resource)
        this.send(bindEl)
    } else if ((this.state === STATE_AUTHED) &&
               !this.did_session &&
               this.streamFeatures.getChild('session', Stanza.NAMESPACE.NS_SESSION)) {
        this.state = STATE_SESSION
        var stanza = new Stanza(
          'iq',
          { type: 'set', to: this.jid.domain, id: IQID_SESSION  }
        ).c('session', { xmlns: Stanza.NAMESPACE.NS_SESSION })
        this.send(stanza)
    } else if (this.state === STATE_AUTHED) {
        /* Ok, we're authenticated and all features have been
           processed */
        /*this.state = STATE_ONLINE
        this.emit('online', { jid: this.jid })*/
    }
}

Client.prototype.doRegister = function() {
    var id = 'register' + Math.ceil(Math.random() * 99999);
    var isForce = this.options.force || false;
    var iq = new Stanza(
        'iq',
        { type: 'set', id: id, to: this.jid.domain }
    ).c('query', { xmlns: Stanza.NAMESPACE.NS_REGISTER , 'force' : isForce })
    .c('username').t(this.jid.user).up()
	.c('name').t(this.options.name).up()
    .c('password').t(this.password)
    this.send(iq)

    var self = this
    var onReply = function(reply) {
        if (reply.is('iq') && (reply.attrs.id === id)) {
            self.removeListener('stanza', onReply);

            if (reply.attrs.type === 'result') {
                /* Registration successful, proceed to auth */
                self.useFeatures()
            } else if(reply.attrs.type === 'error'){
                var err = reply.getChild('error');
                if(err){
                    var errCondition = err.getChildByAttr('xmlns',Stanza.NAMESPACE.NS_STANZAS).name;
                    var errCode = err.getAttr('code');
                    var errType = err.getAttr('type');
                    var errMsg = err.getAttr('message');
                    var error = {
                        'id': id,
                        condition : errCondition,
                        code: errCode,
                        type: errType,
                        message: errMsg
                    }
                   self.emit('error', error);
                }
            }
        }
    }
    this.on('stanza:preauth', onReply)
}

/**
 * returns all registered sasl mechanisms
 */
Client.prototype.getSaslMechanisms = function() {
    return this.availableSaslMechanisms
}

/**
 * removes all registered sasl mechanisms
 */
Client.prototype.clearSaslMechanism = function() {
    this.availableSaslMechanisms = []
}

/**
 * register a new sasl mechanism
 */
Client.prototype.registerSaslMechanism = function(method) {
    // check if method is registered
    if (this.availableSaslMechanisms.indexOf(method) === -1 ) {
        this.availableSaslMechanisms.push(method)
    }
}

/**
 * unregister an existing sasl mechanism
 */
Client.prototype.unregisterSaslMechanism = function(method) {
    // check if method is registered
    var index = this.availableSaslMechanisms.indexOf(method)
    if (index >= 0) {
        this.availableSaslMechanisms = this.availableSaslMechanisms.splice(index, 1)
    }
}

Client.SASL = sasl
Client.JID = JID
Client.Element = Element
Client.Message = Message
Client.Stanza = Stanza
Client.Iq = Iq
Client.Group = Group
Client.Presence = Presence


module.exports = Client
